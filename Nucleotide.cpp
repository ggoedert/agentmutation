
#include "Nucleotide.hpp"
/**
 * @brief Create a nucleotide, if the parent is given store it in the parent's
 * child array at the appropriate location.
 *
 * @param value Value of the nucleotite: 0, 1, 2 or 3.
 * @param parent Pointer to the previous nucleotide in the genome.
 */
Nucleotide::Nucleotide(uint8_t value, Nucleotide* parent)
{
    this->has_mutation = false;
    this->value = value;
    this->parent = parent;
    if (parent)
    {
        parent->child[(int)value] = this;
        this->position = parent->position + 1;
    }
    else
        this->position = -1;

}
/**
 * @brief Set the mutation status of the current nucleotide.
 *
 * @param has_mutation Boolean containing the mutation status.
 */
void Nucleotide::SetHasMutation(bool has_mutation){
    this->has_mutation = has_mutation;
}
/**
 * @brief Check the mutation status of the current nucleotide.
 *
 * @return Boolean containing the mutation status.
 */
bool Nucleotide::GetHasMutation(){
    return has_mutation;
}
/**
 * @brief Get the value of the current nucleotide.
 *
 * @return Value of the nucleotite: 0, 1, 2 or 3.
 */
uint8_t Nucleotide::GetValue(){
    return value;
}
/**
 * @brief Get the position the current nucleotide.
 *
 * @return Integer containing the position.
 */
int32_t Nucleotide::GetPosition(){
    return position;
}
/**
 * @brief Get the Nucleotide pointer to the previous nucleotide.
 *
 * @return Nucleotide pointer that points to previous nucleotide.
 */
Nucleotide* Nucleotide::GetParent(){
    return parent;
}
/**
 * @brief Get the child of the nucleotide.
 * @param index Index of the child in the children array : 0, 1, 2 or 3.
 * @return Nucleotide* pointer to the desired nucleotide's child.
 */
Nucleotide* Nucleotide::GetChild(int index){
    if(index >= 0 && index < 4)
        return child[index];
    else 
        return nullptr;
}
/**
 * @brief Get all the children of the nucleotide.
 * 
 * @return Array of Nucleotide* pointers, each pointer points to a child.
 */
Nucleotide** Nucleotide::GetChildren(){
    return child;
}
/**
 * @brief Get the child of the nucleotide.
 * @param parent Nucleotide pointer that points to the previous nucleotide.
 */
void Nucleotide::SetParent(Nucleotide* parent){
    this->parent = parent;
}
/**
 * @brief Set the position the current nucleotide.
 *
 * @param position Integer containing the position.
 */
void Nucleotide::SetPosition(int32_t position){
    this->position = position;
}
/**
 * @brief Set the value of the current nucleotide.
 *
 * @param value Value of the nucleotite: 0, 1, 2 or 3.
 */
void Nucleotide::SetValue(uint8_t value){
    this->value = value;
}
/**
 * @brief Set the desired child of the current nucleotide.
 *
 * @param child Nucleotide pointer that points to the child.
 * @param index Index of the child in the children array.
 */
void Nucleotide::SetChild(Nucleotide* child, int index){
    if(index >=0 && index < 4)
        this->child[index] = child;
}
/**
 * @brief Free the memory used by the current Nucleotide.
 *
 * 
 */
Nucleotide::~Nucleotide()
{
}