from memory_profiler import profile
import random, sys
import numpy as np
NPARTICLES = 10000
alphabet = {
    "A": "00",
    "U": "01",
    "C": "10",
    "G": "11"
}
alphabet_reverse = {
    "00": "A",
    "01": "U",
    "10": "C",
    "11": "G"
}
# nucleotide number for different viruses
lengths = {
    "sarscov2": 30000,
    "h1n1": 13000
}
@profile
def do_mutation_str(sequence):
    sequence_length = len(sequence)/2 - 1
    pos = random.randint(0, sequence_length)
    sequence_list = list(sequence)
    sequence_list[pos] = str(random.randint(0,1))
    sequence_list[pos+1] = str(random.randint(0,1))
    return ''.join(sequence_list)

@profile
def do_mutation_packbits(sequence):
    unpacked_sequence = np.unpackbits(sequence, axis=None)
    pos = random.randint(0, unpacked_sequence.shape[0]/2)
    unpacked_sequence[pos] = random.randint(0,1)
    unpacked_sequence[pos+1] = random.randint(0,1)
    return np.packbits(unpacked_sequence, axis=None)

@profile
def test_sequence_str():
    sequences_array = np.empty(NPARTICLES, dtype=object)
    sequence = ""
    for j in range(0, NPARTICLES):
        for i in range(0, lengths['sarscov2']):
            first_bin = random.randint(0,1)
            second_bin = random.randint(0,1)
            sequence+= str(first_bin)
            sequence+= str(second_bin)
        sequences_array[j] = sequence
    for i in range(0, NPARTICLES):
        sequences_array[i] = do_mutation_str(sequences_array[i])

@profile
def test_sequence_bitarray():
    sequences_array = np.empty(NPARTICLES, dtype=object)
    for j in range (0,NPARTICLES):
        bit_array = np.empty((2*lengths['sarscov2']), dtype=bool)
        for i in range(0, lengths['sarscov2']):
            first_bin = random.randint(0,1)
            second_bin = random.randint(0,1)
            bit_array[i] = first_bin
            bit_array[i+1] = second_bin
        pack = np.packbits(bit_array, axis=None)
        sequences_array[j] = pack
    for i in range(0, NPARTICLES):
        sequences_array[i] = do_mutation_packbits(sequences_array[i])
#create abstract sequence
@profile
def estipulate_size():
    sequence = ""
    bit_array = np.empty((2*lengths['sarscov2']), dtype=bool)
    for i in range(0, lengths['sarscov2']):
        first_bin = random.randint(0,1)
        second_bin = random.randint(0,1)
        sequence+= str(first_bin)
        sequence+= str(second_bin)
        bit_array[i] = first_bin
        bit_array[i+1] = second_bin


    size_sequence = sys.getsizeof(sequence)
    print("--------String----------")
    print(f"Sequence size in KB: {size_sequence/1024}")
    print(f"Size of {NPARTICLES} sequences in MB: {size_sequence*NPARTICLES/(1024*1024)}")
    print("-------------------------")

    print("-----------numpy array-------")
    size_sequence = sys.getsizeof(bit_array)
    print(f"Sequence size in KB: {size_sequence/1024}")
    print(f"Size of {NPARTICLES} sequences in MB: {size_sequence*NPARTICLES/(1024*1024)}")
    print("-------------------------")

    print("---------numpy packbits-----------")
    pack = np.packbits(bit_array, axis=None)
    size_sequence = sys.getsizeof(pack)
    print(f"Sequence size in KB: {size_sequence/1024}")
    print(f"Size of {NPARTICLES} sequences in MB: {size_sequence*NPARTICLES/(1024*1024)}")
    print("-------------------------")

if __name__ == "__main__":
    test_sequence_bitarray()
    test_sequence_str()