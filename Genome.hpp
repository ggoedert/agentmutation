#ifndef GENOME_HPP
#define GENOME_HPP
#include <iostream>
#include "Nucleotide.hpp"
#include <string.h>
#include <map>
#include <random>
class Genome
{
private:
    std::mt19937 mt{};
    Nucleotide* tail;;
    Nucleotide* head;
    std::map<int,Nucleotide*> mutation_list;
    int genome_length;
    int mutation_number = 0;
    double mutation_rate = 0.5;

    void PrintGenomeAux(Nucleotide* n, bool end);
public:

    Genome(char* g);
    Nucleotide* GetPosition(int32_t pos);
    Nucleotide* GetHead();
    Nucleotide* GetTail();
    int GetMutationRate();
    void SetMutationRate(double mutation_rate);
    int MutatePosition(uint32_t pos, char new_base, int mutation_id);
    int MutationRandomPosition();
    void PrintGenome();
    ~Genome();
};

#endif