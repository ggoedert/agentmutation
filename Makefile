all:
	g++ -c *.cpp
	g++ *.o -lboost_graph -o main 
debug:
	g++ -Wall -c *.cpp
	g++ -o main *.o
clean: rm *.o main
tree_reader:
	g++ -c -Wall tree_reader.cpp
	g++ tree_reader.o -lboost_graph -o tree_reader_main