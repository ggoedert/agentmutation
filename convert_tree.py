import re, os, argparse

def main(filename):
    with open(filename, "r+") as tree:
        buffer = ""
        nid_pattern = "\s+<node id=\"\d+\">"
        nid_attribute  = "  <key attr.name=\"id\" attr.type=\"int\" for=\"node\" id=\"n0\" />\n"
        for line in tree.readlines():
            buffer+=line
            if re.match("<graphml xmlns", line):
                buffer+=nid_attribute
            if re.match(nid_pattern, line):
                match = re.match(nid_pattern, line).group(0)
                id_ = re.findall("\d+", match)[0]
                buffer+=f"      <data key=\"n0\">{id_}</data>\n"
        tree.seek(0)
        tree.write(buffer)
        tree.truncate()
        tree.close()

if __name__ == "__main__":
    main("conf_2_seed_1_tree_0.graphml")